<?php
/**
 * k4 Field Notes plugin for Craft CMS 3.x
 *
 * add a textfield with additional markdown
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4fieldnotes\fields;

use k4\k4fieldnotes\K4FieldNotes;
use k4\k4fieldnotes\assetbundles\k4fieldnotesfieldfield\K4FieldNotesFieldFieldAsset;

use Craft;
use craft\base\ElementInterface;
use craft\base\Field;
use craft\helpers\Db;
use yii\db\Schema;
use craft\helpers\Json;

/**
 * @author    Christian Hiller
 * @package   K4FieldNotes
 * @since     1.0.0
 */
class K4FieldNotesField extends Field
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $noteContent = '';

    // Static Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('k4-field-notes', 'Field Note');
    }

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules = array_merge($rules, [
            ['noteContent', 'string']
        ]);
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function getContentColumnType(): string
    {
        return Schema::TYPE_STRING;
    }

    /**
     * @inheritdoc
     */
    public function normalizeValue($value, ElementInterface $element = null)
    {
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function serializeValue($value, ElementInterface $element = null)
    {
        return parent::serializeValue($value, $element);
    }

    /**
     * @inheritdoc
     */
    public function getSettingsHtml()
    {
        // Render the settings template
        return Craft::$app->getView()->renderTemplate(
            'k4-field-notes/_components/fields/K4FieldNotesField_settings',
            [
                'settings' => $this->getSettings(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function getInputHtml($value, ElementInterface $element = null): string
    {
        $settings = $this->getSettings();
        return $settings['noteContent'];
    }
}
