<?php
/**
 * k4 Field Notes plugin for Craft CMS 3.x
 *
 * add a textfield with additional markdown
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4fieldnotes\assetbundles\k4fieldnotesfieldfield;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Christian Hiller
 * @package   K4FieldNotes
 * @since     1.0.0
 */
class K4FieldNotesFieldFieldAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@k4/k4fieldnotes/assetbundles/k4fieldnotesfieldfield/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/K4FieldNotesField.js',
        ];

        $this->css = [
            'css/K4FieldNotesField.css',
        ];

        parent::init();
    }
}
