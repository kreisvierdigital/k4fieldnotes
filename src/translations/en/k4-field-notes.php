<?php
/**
 * k4 Field Notes plugin for Craft CMS 3.x
 *
 * add a textfield with additional markdown
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

/**
 * @author    Christian Hiller
 * @package   K4FieldNotes
 * @since     1.0.0
 */
return [
    'k4 Field Notes plugin loaded' => 'k4 Field Notes plugin loaded',
];
