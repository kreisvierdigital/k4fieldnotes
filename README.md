# k4 Field Notes plugin for Craft CMS 3.x

add a textfield with additional markdown

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /k4-field-notes

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for k4 Field Notes.

## k4 Field Notes Overview

-Insert text here-

## Configuring k4 Field Notes

-Insert text here-

## Using k4 Field Notes

-Insert text here-

## k4 Field Notes Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Christian Hiller](https://kreisvier.ch)
